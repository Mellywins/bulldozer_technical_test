FROM node:alpine As development

RUN apk add --update curl && \
     rm -rf /var/cache/apk/*
WORKDIR /usr/src/app

COPY package*.json ./

RUN yarn install 

COPY . .

RUN yarn build
